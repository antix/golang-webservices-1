package main

import (
	"fmt"
	"strconv"
)

func main() {
	testJobs := []job{
		job(func(in, out chan interface{}) {
			for i := 0; i < 10; i++ {
				val := strconv.Itoa(i)
				out <- val
			}
		}),
		job(func(in, out chan interface{}) {
			for val := range in {
				strVal := val.(string)
				intVal, _ := strconv.ParseInt(strVal, 10, 0)
				out <- intVal + 1
			}
		}),
		job(func(in, out chan interface{}) {
			for val := range in {
				fmt.Println(val)
			}
		}),
	}
	ExecutePipeline(testJobs...)
	fmt.Scanln()
}
