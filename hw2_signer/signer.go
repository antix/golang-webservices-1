package main

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// ExecutePipeline run pipeline of jobs
func ExecutePipeline(jobs ...job) {
	wg := &sync.WaitGroup{}
	inCh := make(chan interface{})
	outCh := make(chan interface{})
	for jobIndex, jobFunc := range jobs {
		wg.Add(1)
		go func(index int, task job, in, out chan interface{}) {
			defer wg.Done()
			defer close(out)
			//fmt.Printf("[%d]: in is %v and out is %v\n", index, in, out)
			task(in, out)
		}(jobIndex, jobFunc, inCh, outCh)
		inCh = outCh
		outCh = make(chan interface{})
	}
	wg.Wait()
}

// SingleHash считает значение crc32(data)+"~"+crc32(md5(data)) ( конкатенация двух строк через ~), где data - то что пришло на вход (по сути - числа из первой функции)
func SingleHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	md5QuotaCh := make(chan struct{}, 1)
	for val := range in {
		var data string
		switch val.(type) {
		case int:
			data = strconv.Itoa(val.(int))
		case string:
			data = val.(string)
		default:
			fmt.Printf("Don't know what to do with %[1]T; value: %[1]v\n", val)
			continue
		}
		//fmt.Printf("%s SingleHash data %s\n", data, data)
		crc32Md5Ch := make(chan string, 1)
		wg.Add(1)
		go func(data string, out chan<- string, quotaCh chan struct{}) {
			defer wg.Done()
			quotaCh <- struct{}{}
			md5Data := DataSignerMd5(data)
			//fmt.Printf("%s SingleHash md5(data) %s\n", data, md5Data)
			<-quotaCh
			crc32Md5Data := DataSignerCrc32(md5Data)
			//fmt.Printf("%s SingleHash crc32(md5(data)) %s\n", data, crc32Md5Data)
			out <- crc32Md5Data
		}(data, crc32Md5Ch, md5QuotaCh)
		wg.Add(1)
		go func(data string, in <-chan string, out chan<- interface{}) {
			defer wg.Done()
			crc32Data := DataSignerCrc32(data)
			//fmt.Printf("%s SingleHash crc32(data) %s\n", data, crc32Data)
			crc32Md5Data := <-in
			result := crc32Data + "~" + crc32Md5Data
			//fmt.Printf("%s SingleHash result %s\n", data, result)
			out <- result
		}(data, crc32Md5Ch, out)
	}
	wg.Wait()
}

// MultiHash считает значение crc32(th+data)) (конкатенация цифры, приведённой к строке и строки),
//где th=0..5 ( т.е. 6 хешей на каждое входящее значение ), потом берёт конкатенацию результатов в порядке расчета (0..5),
//где data - то что пришло на вход (и ушло на выход из SingleHash)
func MultiHash(in, out chan interface{}) {
	wg := &sync.WaitGroup{}
	for val := range in {
		data := val.(string)
		resultStrings := make([]string, 6)
		intWG := &sync.WaitGroup{}
		mu := &sync.Mutex{}
		for i := 0; i < 6; i++ {
			intWG.Add(1)
			go func(i int, data string) {
				defer intWG.Done()
				crc32Data := DataSignerCrc32(strconv.Itoa(i) + data)
				//fmt.Printf("%s MultiHash: crc32(th+step1)) %d %s\n", data, i, crc32Data)
				mu.Lock()
				resultStrings[i] = crc32Data
				mu.Unlock()
			}(i, data)
		}
		wg.Add(1)
		go func(out chan<- interface{}) {
			defer wg.Done()
			intWG.Wait()
			var result string
			result = strings.Join(resultStrings, "")
			out <- result
		}(out)
	}
	wg.Wait()
}

// CombineResults получает все результаты, сортирует, объединяет отсортированный результат через _ (символ подчеркивания) в одну строку
func CombineResults(in, out chan interface{}) {
	//var i int
	allStrings := make([]string, 0)
	var result string
	for val := range in {
		data := val.(string)
		allStrings = append(allStrings, data)
	}
	sort.Strings(allStrings)
	result = strings.Join(allStrings, "_")
	//fmt.Printf("CombineResults %s\n", result)
	out <- result
}
