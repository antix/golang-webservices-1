package main

//TODO: add httptest.NewServer and SearchClient to each testCases and then merge all Test* functions

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"sort"
	"strconv"
	"strings"
	"testing"
	"time"
)

// Dataset is a set of data
type Dataset struct {
	XMLName xml.Name `xml:"root"`
	Rows    []Row    `xml:"row"`
}

// Row is a row in Dataset
type Row struct {
	ID            int    `xml:"id"`
	GUID          string `xml:"guid"`
	IsActive      bool   `xml:"isActive"`
	Balance       string `xml:"balance"`
	Picture       string `xml:"picture"`
	Age           int    `xml:"age"`
	EyeColor      string `xml:"eyeColor"`
	FirstName     string `xml:"first_name"`
	LastName      string `xml:"last_name"`
	Gender        string `xml:"gender"`
	Company       string `xml:"company"`
	Email         string `xml:"email"`
	Phone         string `xml:"phone"`
	Address       string `xml:"address"`
	About         string `xml:"about"`
	Registered    string `xml:"registered"`
	FavoriteFruit string `xml:"favoriteFruit"`
}

type TestCase struct {
	Request     SearchRequest
	IsError     bool
	ErrorString string
}

var dataset Dataset

func TestMain(m *testing.M) {
	file, err := os.Open("dataset.xml")
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	err = xml.Unmarshal(data, &dataset)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	os.Exit(m.Run())
}

// SearchServer emulates real search server
func SearchServer(w http.ResponseWriter, r *http.Request) {
	if accessToken := r.Header.Get("AccessToken"); accessToken == "" {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	if err := r.ParseForm(); err != nil {
		message := fmt.Sprintf("Error parsing form: %s", err)
		http.Error(w, message, http.StatusInternalServerError)
		return
	}
	query := r.Form.Get("query")
	var answer []User
	limit, err := strconv.Atoi(r.Form.Get("limit"))
	if err != nil {
		message := fmt.Sprintf("Error parsing limit: %s", err)
		http.Error(w, message, http.StatusInternalServerError)
		return
	}
	offset, err := strconv.Atoi(r.Form.Get("offset"))
	if err != nil {
		message := fmt.Sprintf("Error parsing offset: %s", err)
		http.Error(w, message, http.StatusInternalServerError)
		return
	}
	for _, row := range dataset.Rows {
		name := row.FirstName + " " + row.LastName
		if strings.Contains(name, query) || strings.Contains(row.About, query) {
			answer = append(answer, User{
				Id:     row.ID,
				Name:   name,
				Age:    row.Age,
				About:  row.About,
				Gender: row.Gender,
			})
		}
	}

	orderField := r.Form.Get("order_field")
	orderBy, err := strconv.Atoi(r.Form.Get("order_by"))
	if err != nil || -1 > orderBy || orderBy > 1 {
		message, _ := json.Marshal(SearchErrorResponse{Error: "InvalidSortOrder"})
		http.Error(w, string(message), http.StatusBadRequest)
		return
	}
	switch orderField {
	case "Id":
		if orderBy == OrderByAsc {
			sort.SliceStable(answer, func(i, j int) bool { return answer[i].Id < answer[j].Id })
		} else if orderBy == OrderByDesc {
			sort.SliceStable(answer, func(i, j int) bool { return answer[i].Id > answer[j].Id })
		}
	case "Age":
		if orderBy == OrderByAsc {

			sort.SliceStable(answer, func(i, j int) bool { return answer[i].Age < answer[j].Age })
		} else if orderBy == OrderByDesc {
			sort.SliceStable(answer, func(i, j int) bool { return answer[i].Age > answer[j].Age })
		}
	case "Name", "":
		if orderBy == OrderByAsc {
			sort.SliceStable(answer, func(i, j int) bool { return answer[i].Name < answer[j].Name })
		} else if orderBy == OrderByDesc {
			sort.SliceStable(answer, func(i, j int) bool { return answer[i].Name > answer[j].Name })
		}
	default:
		message, _ := json.Marshal(SearchErrorResponse{Error: "ErrorBadOrderField"})
		http.Error(w, string(message), http.StatusBadRequest)
		return
	}
	if offset > len(answer) {
		offset = len(answer)
	}
	if limit == 0 {
		limit = len(answer)
	}
	if offset+limit > len(answer) {
		limit = len(answer) - offset
	}
	answer = answer[offset : limit+offset]
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	err = enc.Encode(answer)
	if err != nil {
		log.Println("Failed to marshal to json:", answer)
	}
}
func TestFindUsers(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	testCases := []TestCase{
		TestCase{
			Request: SearchRequest{
				Limit:      100,
				Offset:     0,
				Query:      "Dillard",
				OrderField: "Id",
				OrderBy:    OrderByAsc,
			},
			IsError:     false,
			ErrorString: "Nope",
		},
		TestCase{
			Request: SearchRequest{
				Limit:      1,
				Offset:     0,
				Query:      "Dillard",
				OrderField: "Id",
				OrderBy:    OrderByAsc,
			},
			IsError:     false,
			ErrorString: "Nope",
		},
		TestCase{
			Request: SearchRequest{
				Limit:      100,
				Offset:     0,
				Query:      "",
				OrderField: "Gender",
				OrderBy:    OrderByAsc,
			},
			IsError:     true,
			ErrorString: fmt.Sprintf("OrderFeld %s invalid", "Gender"),
		},
		TestCase{
			Request: SearchRequest{
				Limit:      -1,
				Offset:     0,
				Query:      "",
				OrderField: "",
				OrderBy:    OrderByAsc,
			},
			IsError:     true,
			ErrorString: "limit must be > 0",
		},
		TestCase{
			Request: SearchRequest{
				Limit:      0,
				Offset:     -1,
				Query:      "",
				OrderField: "",
				OrderBy:    OrderByAsc,
			},
			IsError:     true,
			ErrorString: "offset must be > 0",
		},
		TestCase{
			Request: SearchRequest{
				Limit:      0,
				Offset:     0,
				Query:      "",
				OrderField: "",
				OrderBy:    -2,
			},
			IsError:     true,
			ErrorString: "unknown bad request error",
		},
	}
	for _, testCase := range testCases {
		client := &SearchClient{
			AccessToken: "secret",
			URL:         ts.URL,
		}
		result, err := client.FindUsers(testCase.Request)

		if err == nil && testCase.IsError {
			t.Errorf("Unexpected success: %#v", result)
		}
		if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
			t.Errorf("Unexpected error: %#v", err)
		}
		/*if !testCase.IsError {
			fmt.Printf("Found %d users. Next page: %t\n", len(result.Users), result.NextPage)
			fmt.Printf("%+v\n", result.Users)
		}*/
	}
}

func TestAuth(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	defer ts.Close()

	testCase := TestCase{
		Request: SearchRequest{
			Limit:      100,
			Offset:     0,
			Query:      "",
			OrderField: "",
			OrderBy:    0,
		},
		IsError:     true,
		ErrorString: "Bad AccessToken",
	}

	client := &SearchClient{
		AccessToken: "",
		URL:         ts.URL,
	}
	result, err := client.FindUsers(testCase.Request)

	if err == nil && testCase.IsError {
		t.Errorf("Unexpected success: %#v", result)
	}
	if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
		t.Errorf("Unexpected error: %#v", err)
	}
}

// SearchServerTimeout makes timeout
func SearchServerTimeout(w http.ResponseWriter, r *http.Request) {
	time.Sleep(1500 * time.Millisecond)
	var answer []User
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	enc := json.NewEncoder(w)
	err := enc.Encode(answer)
	if err != nil {
		log.Println("Failed to marshal to json:", answer)
	}
}

func TestTimeout(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(SearchServerTimeout))
	defer ts.Close()

	testCase := TestCase{
		Request: SearchRequest{
			Limit:      100,
			Offset:     0,
			Query:      "",
			OrderField: "",
			OrderBy:    0,
		},
		IsError:     true,
		ErrorString: "timeout for",
	}

	client := &SearchClient{
		AccessToken: "",
		URL:         ts.URL,
	}
	result, err := client.FindUsers(testCase.Request)

	if err == nil && testCase.IsError {
		t.Errorf("Unexpected success: %#v", result)
	}
	if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
		t.Errorf("Unexpected error: %#v", err)
	}
}

func TestUnknownError(t *testing.T) {
	testCase := TestCase{
		Request: SearchRequest{
			Limit:      100,
			Offset:     0,
			Query:      "",
			OrderField: "",
			OrderBy:    0,
		},
		IsError:     true,
		ErrorString: "unknown error1",
	}

	client := &SearchClient{
		AccessToken: "",
		URL:         "http://999.999.999.999/",
		// or URL: "",
	}
	result, err := client.FindUsers(testCase.Request)

	if err == nil && testCase.IsError {
		t.Errorf("Unexpected success: %#v", result)
	}
	if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
		t.Errorf("Unexpected error: %#v", err)
	}
}

func TestBadErrorJSON(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		http.Error(w, "Hello!", http.StatusBadRequest)
	}))
	defer ts.Close()

	testCase := TestCase{
		Request: SearchRequest{
			Limit:      100,
			Offset:     0,
			Query:      "",
			OrderField: "",
			OrderBy:    0,
		},
		IsError:     true,
		ErrorString: "cant unpack error json",
	}

	client := &SearchClient{
		AccessToken: "",
		URL:         ts.URL,
	}
	result, err := client.FindUsers(testCase.Request)

	if err == nil && testCase.IsError {
		t.Errorf("Unexpected success: %#v", result)
	}
	if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
		t.Errorf("Unexpected error: %#v", err)
	}
}

func TestBadResultJSON(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintln(w, "Hello!")
	}))
	defer ts.Close()

	testCase := TestCase{
		Request: SearchRequest{
			Limit:      100,
			Offset:     0,
			Query:      "",
			OrderField: "",
			OrderBy:    0,
		},
		IsError:     true,
		ErrorString: "cant unpack result json",
	}

	client := &SearchClient{
		AccessToken: "",
		URL:         ts.URL,
	}
	result, err := client.FindUsers(testCase.Request)

	if err == nil && testCase.IsError {
		t.Errorf("Unexpected success: %#v", result)
	}
	if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
		t.Errorf("Unexpected error: %#v", err)
	}
}

func TestFatalError(t *testing.T) {
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Everything is sad...", http.StatusInternalServerError)
	}))
	defer ts.Close()

	testCase := TestCase{
		Request: SearchRequest{
			Limit:      100,
			Offset:     0,
			Query:      "",
			OrderField: "",
			OrderBy:    0,
		},
		IsError:     true,
		ErrorString: "SearchServer fatal error",
	}

	client := &SearchClient{
		AccessToken: "",
		URL:         ts.URL,
	}
	result, err := client.FindUsers(testCase.Request)

	if err == nil && testCase.IsError {
		t.Errorf("Unexpected success: %#v", result)
	}
	if err != nil && !strings.Contains(err.Error(), testCase.ErrorString) {
		t.Errorf("Unexpected error: %#v", err)
	}
}
