package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
	"sort"
)

func walkDir(out io.Writer, root string, printFiles bool, lastPrefix string, parentWasLast bool) error {
	f, err := os.Open(root)
	if err != nil {
		return err
	}
	fileInfos, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return err
	}
	sort.Slice(fileInfos, func(i, j int) bool { return fileInfos[i].Name() < fileInfos[j].Name() })

	fileInfosLen := len(fileInfos)
	if printFiles == false {
		var dirsCount int
		for _, file := range fileInfos {
			if file.IsDir() {
				dirsCount++
			}
		}
		fileInfosLen = dirsCount
	}
	var size, nextPath, startSymbol, nextStartSymbol string
	isLast := false
	var i int
	for _, file := range fileInfos {
		if i+1 == fileInfosLen {
			startSymbol = `└`
			nextStartSymbol = ``
			isLast = true
		} else {
			startSymbol = `├`
			nextStartSymbol = `│`
		}

		if file.IsDir() {
			fmt.Fprintf(out, "%s%s───%s\n", lastPrefix, startSymbol, file.Name())
			nextPath = filepath.Join(root, file.Name())
			nextPrefix := fmt.Sprintf("%s%s\t", lastPrefix, nextStartSymbol)
			walkDir(out, nextPath, printFiles, nextPrefix, isLast)
			i++
		} else if printFiles == true {
			if file.Size() > 0 {
				size = fmt.Sprintf("%db", file.Size())
			} else {
				size = "empty"
			}
			fmt.Fprintf(out, "%s%s───%s (%s)\n", lastPrefix, startSymbol, file.Name(), size)
			i++
		}
	}
	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) error {
	err := walkDir(out, path, printFiles, "", false)
	return err
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
